extends KinematicBody


var direction = Vector3.BACK
var puppet_direction =Vector3.BACK
var velocity =  Vector3.ZERO
var puppet_velocity = Vector3.ZERO
var strafe_dir =  Vector3.ZERO
var strafe =  Vector3.ZERO
var position = Vector3.ZERO
var puppet_position = Vector3.ZERO

var aim_turn = 0
var vertical_velocity = 0
var gravity = 20
var movement_speed = 0
var walk_speed = 1.5
var run_speed = 5
var acceleration = 7
var angular_acceleration =7


# Called when the node enters the scene tree for the first time.
func _ready():
	rset_config("puppet_velocity", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	rset_config("puppet_direction", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	rset_config("puppet_position", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	
	direction=Vector3.BACK.rotated(Vector3.UP,$camroot.global_transform.basis.get_euler().y)
	$camroot/h/v/Camera.add_exception(get_parent())
	if is_network_master():
		set_translation(Vector3(5,1,1))
	else:
		set_translation(puppet_position)
		
func _physics_process(delta):
	if is_network_master():
		$Mesh.rotation.y = lerp_angle($Mesh.rotation.y, atan2(direction.x, direction.z) - rotation.y, delta * angular_acceleration)
		acceleration = 5
		var h_rot = $camroot/h.global_transform.basis.get_euler().y
		
		if Input.is_action_pressed("forward") ||  Input.is_action_pressed("backward") ||  Input.is_action_pressed("left") ||  Input.is_action_pressed("right"):
			
			direction = Vector3(Input.get_action_strength("left") - Input.get_action_strength("right"),
						0,
						Input.get_action_strength("forward") - Input.get_action_strength("backward"))

			strafe_dir = direction
			
			direction = direction.rotated(Vector3.UP, h_rot).normalized()
			movement_speed = run_speed
			$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), -1, delta * acceleration))
		else:
			movement_speed = 0
			strafe_dir = Vector3.ZERO
			$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), 0, delta * acceleration))
		
		velocity = lerp(velocity, direction * movement_speed, delta * acceleration)
		position=get_translation()
		rset("puppet_position",position)
		rset("puppet_velocity",velocity)
		rset("puppet_direction",direction)
	else:
		$Mesh.rotation.y = lerp_angle($Mesh.rotation.y, atan2(direction.x, direction.z) - rotation.y, delta * angular_acceleration)
		acceleration = 5
		direction = puppet_direction
		velocity = puppet_velocity
		set_translation(puppet_position)
		strafe_dir = direction
		movement_speed = run_speed
		$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), -1, delta * acceleration))
	move_and_slide(velocity + Vector3.DOWN * vertical_velocity, Vector3.UP)
	if !is_on_floor():
		vertical_velocity += gravity * delta
	else:
		vertical_velocity = 0
	

	strafe = lerp(strafe, strafe_dir + Vector3.RIGHT * 1, delta * acceleration)
	
	var iw_blend = (velocity.length() - walk_speed) / walk_speed
	var wr_blend = (velocity.length() - walk_speed) / (run_speed - walk_speed)
	if velocity.length() <= walk_speed:
		$AnimationTree.set("parameters/iwr_blend/blend_amount" , iw_blend)
	else:
		$AnimationTree.set("parameters/iwr_blend/blend_amount" , wr_blend)
	
	aim_turn = 0
