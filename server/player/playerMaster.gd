extends Spatial


var position = Vector3()
var puppet_position = Transform()

func _ready():
	rset_config("puppet_position", MultiplayerAPI.RPC_MODE_REMOTESYNC)

func _process(delta):
	if is_network_master():
		position=get_global_transform()
		rset("puppet_position",position)
	else:
		position=puppet_position
		$Supervisor,zzz
