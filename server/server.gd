# Typical lobby implementation; imagine this being in /root/lobby.

extends Node

# Connect all functions

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	rset_config("register_player", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	
	loadWorld()
# Player info, associate ID to data
var player_info = {}
# Info we send to other players
var my_info = { name = "server", favorite_color = Color8(255, 0, 0) }

func _player_connected(id):
	if(id!=1):
		rpc_id(id, "register_player", my_info)

func _player_disconnected(id):
	player_info.erase(id) # Erase player from info.
	get_node("/root/root/lobby/world/players/"+str(id)).free()
	
func _connected_ok():
	pass

func loadWorld():
	var world= preload("res://world/world.tscn").instance()
	get_node("/root/root/lobby").call_deferred("add_child",world)



remote func register_player(info):
	var id = get_tree().get_rpc_sender_id()
	if(id!=1):
		var client=preload("res://player/player.tscn").instance()
		client.set_network_master(id)
		client.set_name(str(id))
		get_node("/root/root/lobby/world/players").add_child(client)
		


