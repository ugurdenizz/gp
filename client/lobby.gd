extends Node

var selfNode=false


func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	rset_config("register_player", MultiplayerAPI.RPC_MODE_REMOTESYNC)
# Player info, associate ID to data
var player_info = {}
# Info we send to other players0
var my_info = { name = str(rand_range(0,999)), favorite_color = Color8(0, 0, 255) }

func _player_connected(id):
	rpc_id(id, "register_player", my_info)
	print("i am called")	
#	register_player(id,my_info)

func _player_disconnected(id):
	player_info.erase(id) # Erase player from info.
	get_node("/root/root/lobby/world/players/"+str(id)).free()

func _connected_ok():
	loadWorld()

func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.

func loadWorld():
	var world= preload("res://world/world.tscn").instance()
	get_node("/root/root/lobby").add_child(world)
	

remote func register_player(id):
	var selfPeerID = get_tree().get_network_unique_id()
	id = get_tree().get_rpc_sender_id()
	if(id!=1&&id!=selfPeerID):
		var anotherclient=load("res://player/player.tscn").instance()
		anotherclient.set_network_master(id)
		anotherclient.set_name(str(id))
		print(id)
		get_node("/root/root/lobby/world/players").add_child(anotherclient)
		print("loaded myself for no reason ",selfPeerID)
	print("i am executed")
	if(selfNode==true):
		#set as master of self
		var meMyself=get_node("/root/root/lobby/world/players/"+str(selfPeerID))
		meMyself.set_network_master(selfPeerID)
		print("i have reset the network master with id ",selfPeerID)
	else:
		#loadsself
		print("loaded myself",selfPeerID)
		var meMyself = load("res://player/player.tscn").instance()
		meMyself.set_name(str(selfPeerID))
		meMyself.set_network_master(selfPeerID) # Will be explained later
		get_node("/root/root/lobby/world/players").add_child(meMyself)
		selfNode=true
	
#
	
