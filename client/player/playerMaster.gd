extends Spatial


var position
var puppet_position = Transform()

func _ready():
	rset_config("puppet_position", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	if is_network_master():
		position=$Supervisor.set_translation(Vector3(1,1,1))
	else:
		$Supervisor.set_transform(puppet_position)	
		print("i am called on client side2!")

func _process(delta):
	if is_network_master():
		position=$Supervisor.get_translation()
		print(position)
		rset("puppet_position",position)
		print("pos set")
